#include "Arduino.h"
#include "config.h"
#include "FocheggiatoreLib.h"
#include <AMIS30543.h>
#include <EEPROM.h>
#include <SPI.h>

/*
 * TODO
 * - Abilitare / disabilitare motore per non consumare corrente
 * - Verificare i limiti delle configurazioni
 * - Verificare controllo Limit
 * - Inserire offset di home
 */



/*
 * CONSTRUCTOR
 */
Focheggiatore::Focheggiatore(int MOT_amisDirPin, int MOT_amisStepPin, int MOT_amisSlaveSelect, int MOT_switchFocPri, int MOT_switchFocSec, int MOT_switchHomeFoc )
{
<<<<<<< HEAD
      this->amisDirPin = MOT_amisDirPin;
      this->amisStepPin = MOT_amisStepPin;
      this->amisSlaveSelect = MOT_amisSlaveSelect;
=======
  	  this->amisDirPin = MOT_amisDirPin;
  	  this->amisStepPin = MOT_amisStepPin;
  	  this->amisSlaveSelect = MOT_amisSlaveSelect;
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
      this->switchFocPri = MOT_switchFocPri;
      this->switchFocSec = MOT_switchFocSec;
      this->switchHome = MOT_switchHomeFoc;
}




/*
 *  INIZIALIZZAZIONE
 */
void Focheggiatore::begin()
{    
      // Ottengo i dati dalla eeprom
      getEeprom();
    
      // Inizializzo le variabili di servizio
      appo                  = 0;
      count                 = 0;
      timeH                 = 0;
      timeL                 = 0;
      timeDelay             = 50;
      limit                 = false;
      backlashCounter       = backlash;
      oldDir                = 0;
      infinity              = 0;
      currentMicros         = 0;
      currentMillis         = 0;
      rampTimeMicro         = 0;
      rampTimeRatio         = 0;
      targetStep            = 0;
      oldError              = 1;
      statusError           = 1;
      checkErrorPrec        = 0;
      status                = STATUS_STOP;
    
    
      // Inizializzo i PIN
      pinMode(amisDirPin, OUTPUT); 
      pinMode(amisStepPin, OUTPUT);
      pinMode(switchFocPri, INPUT);
      pinMode(switchFocSec, INPUT); 
      pinMode(switchHome, INPUT); 
    
      // Inizializzo il Driver Amis
      SPI.begin();
      stepper.init(amisSlaveSelect);
      delay(1);
      stepper.resetSettings();
<<<<<<< HEAD
      stepper.setCurrentMilliamps(CONF_FOC_CURRENT_LIMIT); // LEONARDO MODIFICA 1000 o 600
      stepper.setStepMode( CONF_FOC_MICROSTEP );
      stepper.enableDriver();
      
      //digitalWrite(amisStepPin, LOW);  
      cbi(PORTB, amisStepPin);
      
      //digitalWrite(amisDirPin, LOW);
      cbi(PORTB, amisDirPin);
=======
      stepper.setCurrentMilliamps(1400); // LEONARDO MODIFICA 1000 o 600
      stepper.setStepMode( CONF_FOC_MICROSTEP );
      stepper.enableDriver();
      
      digitalWrite(amisStepPin, LOW);  
      digitalWrite(amisDirPin, LOW);
  
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1

}



/*
 * MOVIMENTAZIONE MOTORE
 */
void Focheggiatore::move( char dirLoc, long int stepOut, int inf = 0 )
{
<<<<<<< HEAD

        initRampData();
                    
=======
        initRampData();
                              
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
        if( dirLoc == KEY_FORWARD){
          homePosition = false;
          status = KEY_FORWARD;
          setStepDirection(1);
          dir = 1;
        }
        else if ( dirLoc == KEY_BACKWARD){
          homePosition = false;
          status = KEY_BACKWARD;
          setStepDirection(0);
          dir = 0;
        }
        else if( dirLoc == KEY_HOME ){
          homePosition = true;
          status = KEY_BACKWARD;
          infinity = 1;
          setStepDirection(1);
          dir = 1;
          targetStep = 1000 * CONF_FOC_MICROSTEP;
<<<<<<< HEAD

=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
          return;
        }

        if( inf ){
          infinity = 1;
        }else{
          infinity = 0;
        }

        if( oldDir != dir ){
          stepOut += backlash;
        }
        
        targetStep = CONF_FOC_MICROSTEP * stepOut;
        //stepper.enableDriver();
}




/*
 * GOTO 
 */
void Focheggiatore::goTo( long int posLoc )
{
      long int stepOut = posLoc - steps;
      long int modStepOut = abs( stepOut );

      // Se viene richiesta la stessa posizione non muovo nulla
      if( posLoc < 2 ) return;
      
      if( steps > posLoc ) move( KEY_BACKWARD, modStepOut ); 
      else move( KEY_FORWARD, modStepOut );
}





/*
 * MOTOR STOP
 */
void  Focheggiatore::stop()
<<<<<<< HEAD
{ 
      
=======
{
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
      if( !limit ){
        if( oldDir != dir ){
          if( dir > 0 ){
          steps -= backlash;
          }
          else{
            steps += backlash;
          }
        }
      }
      limit = false;
      oldDir = dir;
      targetStep = 0;
      EEPROMWriteInt( 4, steps );
      EEPROMWriteInt( 10, oldDir );
      status = STATUS_STOP;
      infinity = 0;
      //stepper.disableDriver();
<<<<<<< HEAD
}





/*
 * MOVIMENTAZIONE STEP
 */
void Focheggiatore::singleSteps ()
{ 
        currentMillis = millis();
        currentMicros = micros();
        // Controllo la presenza di errori ogni 2 sec
        if( currentMillis - checkErrorPrec > 2000 ){
          readError();
          checkErrorPrec = currentMillis;
        }
        // Se sono fermo non faccio nulla
        if( status == STATUS_STOP ){
          return;
        }
        

        
        // Se sto cercando la Home verifico di averla trovata
        if( homePosition ){
          isInHome();
        }
        
        // Verifico di non aver raggiunto un limite fisico impostato
        if( limitS( dir ) ){
          stop();
        }
        
        // Gestisco la fine del movimento. Se non infinto mi fermo altrimenti rigenero gli step traget
        if( targetStep == 0 && infinity ){
          targetStep = 1000 * CONF_FOC_MICROSTEP;
          return;
        }else if( targetStep == 0 && !infinity ){
          stop();
        }
    
    
        // Controllo il driver
        if( timeH == 0 && timeL == 0){
          sbi(PORTB, amisStepPin);
          //digitalWrite(amisStepPin, HIGH);
          timeH = currentMicros;
          timeL = 0;
          return;
        }   
        if( timeH!=0 && currentMicros - timeH > CONF_FOC_LOW_MICRO_TIME ){
            cbi(PORTB, amisStepPin);
            //digitalWrite(amisStepPin, LOW);
            timeH = 0;
            timeL = currentMicros;       
            return;      
        }
       if( timeL!=0 && (currentMicros - timeL > speedDelay  ) ){
            timeH = 0;
            timeL = 0;
            targetStep--;
            appo++;
            if(appo == CONF_FOC_MICROSTEP){
    
              if( dir == 1 ){
                steps--; 
              }
              else{
                steps++;
              }
              
              appo = 0;
    
              if( rampTimeStart && speedDelay > maxSpeed ){
                  rampTimeGap = currentMicros - rampTimeStart;
                  speedDelay = initialSpeed - (int)( rampTimeGap * rampRatio ); 
              }else{
                speedDelay = maxSpeed;
              }
          
            return;
            }
        
        }
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
}





<<<<<<< HEAD
=======
/*
 * MOVIMENTAZIONE STEP
 */
void Focheggiatore::singleSteps ()
{ 
        // Se sono fermo non faccio nulla
        if( status == STATUS_STOP ) return;
    
        currentMillis = millis();
        currentMicros = micros();
        
        // Controllo la presenza di errori ogni 2 sec
        if( currentMillis - checkErrorPrec > 2000 ){
          readError();
          checkErrorPrec = currentMillis;
        }
        
        // Se sto cercando la Home verifico di averla trovata
        if( homePosition ){
          isInHome();
        }
        
        // Verifico di non aver raggiunto un limite fisico impostato
        if( limitS( dir ) ){
          stop();
        }
    
        // Gestisco la fine del movimento. Se non infinto mi fermo altrimenti rigenero gli step traget
        if( targetStep == 0 && infinity ){
          targetStep = 1000 * CONF_FOC_MICROSTEP;
          return;
        }else if( targetStep == 0 && !infinity ){
          stop();
          return;
        }
    
    
        // Controllo il driver
        if( timeH == 0 && timeL == 0){
          digitalWrite(amisStepPin, HIGH);
          timeH = currentMicros;
          timeL = 0;
          return;
        }   
        if( timeH!=0 && currentMicros - timeH > CONF_FOC_LOW_MICRO_TIME ){
            digitalWrite(amisStepPin, LOW);
            timeH = 0;
            timeL = currentMicros;       
            return;      
        }
       if( timeL!=0 && (currentMicros - timeL > speedDelay  ) ){
            timeH = 0;
            timeL = 0;
            targetStep--;
            appo++;
            if(appo == CONF_FOC_MICROSTEP){
    
              if( dir == 1 ) steps++; 
              else steps--;
              
              appo = 0;
    
              if( rampTimeStart && speedDelay > maxSpeed ){
                  rampTimeGap = currentMicros - rampTimeStart;
                  speedDelay = initialSpeed - (int)( rampTimeGap * rampRatio ); 
              }else{
                speedDelay = maxSpeed;
              }
          
            return;
            }
        
        }
}





>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1

/*
 * SET DIRECTION
 */
void Focheggiatore::setStepDirection( bool dirLoc )
{
      stepper.setDirection( dirLoc );
} 




/*
 * READ CURRENT MOTOR POSIZTION
 */
void Focheggiatore::readPosition()
{ 
      Serial.println(steps);
}



/*
 * READ CURRENT MOTOR STATUS
 */
void Focheggiatore::readStatus()
{  
      Serial.println(status);
}



/*
 * READ MOTOR STATUS AND POSITION
 */
void Focheggiatore::readAll()
{
      Serial.print(status);
      Serial.print(" ");
      Serial.println(steps);
}




/*
 * CHECK PHISICAL LIMITS
 */
bool Focheggiatore::limitS( bool dirLoc)
{
       if( !digitalRead( switchFocPri ) && ( dir == 0 ) ){
<<<<<<< HEAD
          limit = true;
          homePosition = false;
          status = STATUS_STOP;
          Serial.println("stopS");
          return true;
          
=======
          steps = 0;
          limit = true;
          homePosition = false;
          status = STATUS_STOP;
          return true;
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
       }
       else if( !digitalRead( switchFocSec ) && ( dir == 1 ) ){
          steps = 0;
          limit = true;
          homePosition = false;
          status = STATUS_STOP;
<<<<<<< HEAD
          Serial.println("stopG");
          return true;
          
=======
          return true;
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
       }
       else{
        return false;
       }
}




/*
 *  CHECK IF MOTOR IS IN HOME POSITION
 */
void Focheggiatore::isInHome()
{
      if( !digitalRead( switchHome ) ){
        steps = 0;
        limit = true;
        homePosition = false;
        infinity = 0;
<<<<<<< HEAD
        Serial.println("qua");
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
        stop();
      }
}



/*
 *  READ ALL EEPROM SETTINGS
 */
void Focheggiatore::getEeprom()
{  
      reloadEeprom();
      Serial.print("max: ");
      Serial.print(maxSpeed);
      Serial.print("| ini: ");
      Serial.print(initialSpeed);
      Serial.print("| steps: ");
      Serial.print(steps);
      Serial.print("| backlash: ");
      Serial.print(backlash);
      Serial.print("| rampTime: ");
      Serial.println(rampTime);
}



/*
 *  LOAD EEPROM TO ROUTIN DATA
 */
void Focheggiatore::reloadEeprom()
{  
      maxSpeed      = EEPROMReadInt( 0 );
      initialSpeed  = EEPROMReadInt( 2 );  
      steps         = EEPROMReadInt( 4 );
      backlash      = EEPROMReadInt( 6 );
      rampTime      = EEPROMReadInt( 8 );
}



/*
 * SET EEPROM MAX SPEED
 */
void Focheggiatore::setEepromMaxVel( int value )
<<<<<<< HEAD
{ 
      //limite fisico sulla velocitÃ 
      if( value < LIMIT_FOC_MAX_SPEED ){
        value = LIMIT_FOC_MAX_SPEED;
      }
      //se la velocitÃ  massima Ã¨ maggiore dell'iniziale, prendo l'iniziale, sennÃ² rallenterebbe
      if( value > LIMIT_FOC_INI_SPEED ){
        value = LIMIT_FOC_INI_SPEED;
      }
=======
{
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
      EEPROMWriteInt( 0, value );
      maxSpeed = value;
}



/*
 * SET EEPROM INITIAL SPEED
 */
void Focheggiatore::setEepromIniVel( int value )
{  
<<<<<<< HEAD
      //limite fisico sulla velocitÃ  iniziale
      if( value < LIMIT_FOC_INI_SPEED ){
        value = LIMIT_FOC_INI_SPEED;
      }
      //se la velocitÃ  iniziale Ã¨ minore della massima, prendo la massima, sennÃ² rallenterebbe      
      if( value < LIMIT_FOC_MAX_SPEED ){
        value = LIMIT_FOC_MAX_SPEED;
      }
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
      EEPROMWriteInt( 2, value );
      initialSpeed = value;
}



/*
 * SET EEPROM BACKSLASH
 */
void Focheggiatore::setEepromBacklash( int value )
{  
      EEPROMWriteInt( 6, value );
      backlash = value; 
}



/*
 * SET EEPROM ACCELERATION TIME
 */
void Focheggiatore::setEepromRampTime( int value )
{
      EEPROMWriteInt( 8, value );
      rampTime = value;
<<<<<<< HEAD
}



/*
 * SET EEPROM CURRENT POSITION
 */
void Focheggiatore::setEepromOldDir( int value )
{
      EEPROMWriteInt( 10, value );
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
}



/*
<<<<<<< HEAD
=======
 * SET EEPROM CURRENT POSITION
 */
void Focheggiatore::setEepromOldDir( int value )
{
      EEPROMWriteInt( 10, value );
}



/*
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
 * WRITE INT TO EEPROM
 */
void Focheggiatore::EEPROMWriteInt(int address, int value)
{
  
      byte two = (value & 0xFF);
      byte one = ((value >> 8) & 0xFF);

      EEPROM.write(address, two);
      EEPROM.write(address + 1, one);
      
}



/*
 * READ INT FROM EEPROM
 */
int Focheggiatore::EEPROMReadInt(int address)
{
      int two = EEPROM.read(address);
      int one = EEPROM.read(address + 1);
      return ((two << 0) & 0xFF) + ((one << 8) & 0xFFFF);
}



/*
 * CHECK DRIVER ERRORS
 */
/*bool Focheggiatore::checkError(){
  statusError = digitalRead( error );
  if( oldError != statusError ){
    oldError = statusError;
    return true;
  }
  return false;
}*/



/*
 * INIT ACCELERATION DATA
 */
void Focheggiatore::initRampData()
{
      rampRatio = 0;
      rampTimeMicro = 0;
      rampTimeStart = 0;
      
      // Se non ho un tempo di rampa ritorno
      if( rampTime == 0 ){
        speedDelay = maxSpeed; 
      }else{
        speedDelay = initialSpeed; 
      }
  
      // Trasformo il valore salvato in secondi in microsecondi
      unsigned long int rampTimeMicro = rampTime*1000000; 
  
<<<<<<< HEAD
      // Ottengo il gap di velocitÃ  (inteso in microsecondi di frequenza) da dover compensare
      long int speedGap = initialSpeed - maxSpeed;
        
      // Se non c'Ã¨ molta differenza tra le velocitÃ  ritorno
=======
      // Ottengo il gap di velocità (inteso in microsecondi di frequenza) da dover compensare
      long int speedGap = initialSpeed - maxSpeed;
        
      // Se non c'è molta differenza tra le velocità ritorno
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
      if( speedGap < 20 ) return;
  
      //Calcolo il rapporto tra temp T1 (tempo di avvio della rampa) e il T2 (tempo di down del segnale di controllo)
      rampRatio = (double)speedGap / (double)rampTimeMicro;
  
      rampTimeStart = micros();
}



/*
 * READ ERROR FROM DRIVER
 */
void Focheggiatore::readError()
{
<<<<<<< HEAD
=======
      return;
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
      uint16_t flags = stepper.readNonLatchedStatusFlags();
      if (flags)
      {
        if (flags & AMIS30543::OPENY)
        {
                Serial.println("OPENY");
        }
        if (flags & AMIS30543::OPENX)
        {
                Serial.println("OPENX");
        }
        if (flags & AMIS30543::WD)
        {
                Serial.println("WD");
        }
        if (flags & AMIS30543::CPFAIL)
        {
                Serial.println("CPFAIL");
        }
        if (flags & AMIS30543::TW)
        {
                Serial.println("TW");
        }
      }
}



<<<<<<< HEAD


=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
