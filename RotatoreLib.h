#ifndef ROTATORE_H
#define ROTATORE_H
#include <EEPROM.h>
#include <AMIS30543.h>
#include <SPI.h>
#include <Arduino.h>


class Rotatore
{
  public:
    Rotatore(int MOT_amisDirPin1, int MOT_amisStepPin1, int MOT_amisSlaveSelect1, int MOT_switchHome );    
                    
  void begin();  
  void singleSteps();
  void move( char dir, long int stepOut );
  void goTo( long int posLoc );
  void moveInf( char dir );
  void stop();
  void setStepDirection( bool dir );
  void readStatus();
  void readAll();
  void readPosition();
  void isInHome();
  void getEeprom();
  void reloadEeprom();
  void initRampData();
  void setEepromMaxVel( int value );
  void setEepromIniVel( int value );
  void setEepromRampTime( int value );
  void setEepromBacklash( int value );
  void setEepromOldDir( int value );
  void EEPROMWriteInt(int address, int value);
  int EEPROMReadInt(int address);
  long int  checkWire(long int stepsCheck, bool dirLoc);
  //bool checkError();
  void readError();
  
  private:

  uint8_t amisDirPin, amisStepPin, amisSlaveSelect;
  uint8_t switchFocPri, switchFocSec, switchHome;
  AMIS30543 stepper;
  int appo;
  long int count;
  unsigned long int timeH;
  unsigned long int timeL;
  unsigned long int currentMicros;
  unsigned long int currentMillis;
  unsigned long int timeDelay;
  unsigned long int speedDelay;
  unsigned long int initialSpeed;
<<<<<<< HEAD

  unsigned long int maxSpeed;

  long int steps;
  char status;
  long int targetStep;
  bool homePosition;
  bool dir;
=======
  double rampRatio;
	unsigned long int maxSpeed;
  unsigned long int rampCounter;
	long int steps;
	char status;
	long int targetStep;
	bool homePosition;
	bool dir;
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
  bool oldDir; 
  int SET_STEPS;
  int backlash;
  bool limit;
  int backlashCounter;
  int infinity;
 
  //error
  uint16_t oldError;
  uint16_t statusError;
  unsigned long int checkErrorPrec;
<<<<<<< HEAD

  int rampTime;
  double rampRatio;
  float rampTimeRatio;
  unsigned long int rampTimeGap;
  unsigned long int rampTimeStart;
  unsigned long int rampTimeMicro;
  
=======
  int infinity;
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
};


#endif


<<<<<<< HEAD


=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
