#include "Arduino.h"
#include "config.h"
#include "RotatoreLib.h"
#include <AMIS30543.h>
#include <EEPROM.h>
#include <SPI.h>

Rotatore::Rotatore(int MOT_amisDirPin, int MOT_amisStepPin, int MOT_amisSlaveSelect, int MOT_switchHome )
{
	  this->amisDirPin = MOT_amisDirPin;
	  this->amisStepPin = MOT_amisStepPin;
	  this->amisSlaveSelect = MOT_amisSlaveSelect;
    this->switchHome = MOT_switchHome;
}

void Rotatore::begin()
{  
<<<<<<< HEAD
      // Ottengo i dati dalla eeprom
      getEeprom();
    
      // Inizializzo le variabili di servizio
      appo                  = 0;
      count                 = 0;
      timeH                 = 0;
      timeL                 = 0;
      timeDelay             = 50;
      limit                 = false;
      backlashCounter       = backlash;
      oldDir                = 0;
      currentMicros         = 0;
      currentMillis         = 0;
      rampTimeMicro         = 0;
      rampTimeRatio         = 0;
      targetStep            = 0;    
      oldError              = 1;
      statusError           = 1;
      checkErrorPrec        = 0;
      status                = STATUS_STOP;  

      // Inizializzo i PIN
      pinMode(amisDirPin, OUTPUT); 
      pinMode(amisStepPin, OUTPUT);
      pinMode(switchHome, INPUT); 
    
      // Inizializzo il Driver Amis
      SPI.begin();
      stepper.init(amisSlaveSelect);
      delay(1);
      stepper.resetSettings();
      stepper.setCurrentMilliamps(CONF_ROT_CURRENT_LIMIT); // LEONARDO MODIFICA 1000 o 600
      stepper.setStepMode(CONF_FOC_MICROSTEP);
      stepper.enableDriver();

      //digitalWrite(amisStepPin, LOW);  
      cbi(PORTB, amisStepPin);
=======
  pinMode(amisDirPin, OUTPUT); 
  pinMode(amisStepPin, OUTPUT);
  pinMode(switchHome, INPUT); 

  SET_STEPS = 1;
  GIRO_COMPLETO = 3000;
  
  SPI.begin();
  stepper.init(amisSlaveSelect);
  delay(1);
  stepper.resetSettings();
  stepper.setCurrentMilliamps(600);
  stepper.setStepMode(SET_STEPS);
  stepper.enableDriver();

  digitalWrite(amisStepPin, LOW);  
  digitalWrite(amisDirPin, LOW); 
  appo = 0;
  count = 0;
  timeH = 0;
  timeL = 0;
  timeDelay = 50;

  maxSpeed = 50;
  initialSpeed = 300;  
  backlash = 50;
  limit = false;
  backlashCounter = backlash;
  rampCounter = 0;
  rampTime = 2;
  oldDir = 0;
  getEeprom();
  speedDelay  = initialSpeed;

  status = 's';
  targetStep = 0;
  infinity = 0;
    
  oldError = 1;
  statusError = 1;
  checkErrorPrec = 0;
  
}


void Rotatore::singleSteps (){ 

    if( millis() - checkErrorPrec > 1000 ){
      readError();
      checkErrorPrec = millis();
    }
    /*if( checkError() ){
      Serial.println("error");
      readError();
      stop();
    }*/
    if( homePosition ){
      isInHome();
    }
    if( status == 's' ){
      return;
    } 

    
    if( targetStep == 0 && infinity == 0 ){
      Serial.println("NO INF - STOP");
      Serial.println(steps);
      stop();
      return;
    } else if( targetStep == 0 && infinity == 1 ){
      Serial.println("INF REGEN");
      Serial.println(steps);
      targetStep == 1000;
      targetStep *= SET_STEPS;
      return;
    }

    if( timeH == 0 && timeL == 0){
      digitalWrite(amisStepPin, HIGH);
      timeH = micros();
      timeL = 0;
      return;
    }   
    if( timeH!=0 && micros() - timeH > 5){
        digitalWrite(amisStepPin, LOW);
        timeH = 0;
        timeL = micros();       
        return;      
    }
   if( timeL!=0 && (micros() - timeL > speedDelay ) ){
        timeH = 0;
        timeL = 0;
        targetStep--;
        appo++;
        if(appo == SET_STEPS){
          if( dir == 1 ){
            steps++; 
          }
          else {
            steps--;
          }
          appo = 0;
          rampCounter ++;
          if( speedDelay > maxSpeed && rampCounter == 2 ){
                speedDelay --;
                rampCounter = 0;
          } 
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
      
      //digitalWrite(amisDirPin, LOW);
      cbi(PORTB, amisDirPin);

  
}

<<<<<<< HEAD
=======


void Rotatore::move( char dirLoc, long int stepOut ){
          Serial.println(stepOut);
          rampCounter = 0;
          speedDelay = initialSpeed;                    
          if( dirLoc == 'f'){
            homePosition = false;
            status = 'f';
            setStepDirection(1);
            dir = 1;
          }
          else if ( dirLoc == 'b'){
            homePosition = false;
            status = 'b';
            setStepDirection(0);
            dir = 0;
          }
          else if( dirLoc == 'h'){
            homePosition = true;
            dir = 0;
            stepOut = 100;
            if( oldDir != dir ){
              stepOut += backlash;
            }
            status = 'b';
            setStepDirection(0);
            
            targetStep = stepOut * SET_STEPS;
            return;
          }
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1



/*
 * MOVIMENTAZIONE MOTORE
 */
void Rotatore::move( char dirLoc, long int stepOut ){

        initRampData();
               
        if( dirLoc == KEY_FORWARD){
          homePosition = false;
          status = KEY_FORWARD;
          setStepDirection(1);
          dir = 1;
        }
        else if( dirLoc == KEY_BACKWARD){
          homePosition = false;
          status = KEY_BACKWARD;
          setStepDirection(0);
          dir = 0;
        }
        else if( dirLoc == KEY_HOME){
          homePosition = true;
          status = KEY_BACKWARD;
          setStepDirection(0);
          dir = 0;
          stepOut = 100;
          if( oldDir != dir ){
            stepOut += backlash;
          }
<<<<<<< HEAD
          targetStep = stepOut * CONF_FOC_MICROSTEP;
          return;
        }

        if( oldDir != dir ){
          stepOut += backlash;
        }
        
        targetStep = checkWire( stepOut, dir );
        targetStep *= CONF_FOC_MICROSTEP;
}




/*
 * GOTO 
 */
=======
          targetStep = checkWire( stepOut, dir );
          targetStep *= SET_STEPS;
          infinity = 0;
}


>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::goTo( long int posLoc ){
      long int stepOut = posLoc - steps;
      long int modStepOut = abs( stepOut );
      if( steps > posLoc ){
        move( KEY_BACKWARD, modStepOut ); 
      }
      else{
        move( KEY_FORWARD, modStepOut );
      }
}



<<<<<<< HEAD

/*
 * MOVE INFINITY 
 */
void Rotatore::moveInf( char dirLoc ){
          initRampData();
          
          homePosition =  false;         
          if( dirLoc == KEY_FORWARD ){           
            status = KEY_FORWARD;
=======
void Rotatore::moveInf( char dirLoc ){
          homePosition =  false;
          rampCounter = 0;
          speedDelay = initialSpeed;   

                           
          if( dirLoc == 'f' ){
            
            status = 'f';
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
            setStepDirection(1);
            dir = 1;
            targetStep = ( ( COMPLETE_ROTATE / 2 ) + 1 ) - steps;
            if( oldDir != dir ){
              targetStep += backlash;
            }
          }
          else{            
            status = KEY_BACKWARD;
            setStepDirection(0);
            dir = 0;
            targetStep = ( ( COMPLETE_ROTATE / 2 ) - 1 ) + steps;
            if( oldDir != dir ){
              targetStep += backlash;
            }
          }
<<<<<<< HEAD
          targetStep *= CONF_FOC_MICROSTEP;
=======
          targetStep *= SET_STEPS;
          Serial.println(targetStep);
          infinity = 1;
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
}



<<<<<<< HEAD

/*
 * MOTOR STOP
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void  Rotatore::stop(){
  if( !limit ){
    if( oldDir != dir ){
      if( dir > 0 ){
      steps -= backlash;
      }
      else{
        steps += backlash;
      }
    }
  }
  limit = false;
  oldDir = dir;
  targetStep = 0;
  EEPROMWriteInt( 16, steps );
  EEPROMWriteInt( 22, oldDir );
<<<<<<< HEAD
  status = STATUS_STOP;
}




/*
 * MOVIMENTAZIONE STEP
 */
void Rotatore::singleSteps (){ 

        // Se sono fermo non faccio nulla
        if( status == STATUS_STOP ){
          return;
        } 
        currentMillis = millis();
        currentMicros = micros();

        // Controllo la presenza di errori ogni 2 sec        
        if( currentMillis - checkErrorPrec > 2000 ){
          readError();
          checkErrorPrec = currentMillis;
        }
        
        // Se sto cercando la Home verifico di averla trovata
        if( homePosition ){
          isInHome();
        }

        if( targetStep == 0 ){
          stop();
          return;
        } 

        if( timeH == 0 && timeL == 0){
          sbi(PORTB, amisStepPin);
          //digitalWrite(amisStepPin, HIGH);
          timeH = currentMicros;
          timeL = 0;
          return;
        }   
        if( timeH!=0 && currentMicros - timeH > CONF_FOC_LOW_MICRO_TIME){
            cbi(PORTB, amisStepPin);
            //digitalWrite(amisStepPin, LOW);
            timeH = 0;
            timeL = currentMicros;       
            return;      
        }
       if( timeL!=0 && (currentMicros - timeL > speedDelay ) ){
            timeH = 0;
            timeL = 0;
            targetStep--;
            appo++;
            if(appo == CONF_FOC_MICROSTEP){
              if( dir == 0 ){
                steps--; 
              }
              else {
                steps++;
              }
              
              appo = 0;

              if( rampTimeStart && speedDelay > maxSpeed ){
                  rampTimeGap = currentMicros - rampTimeStart;
                  speedDelay = initialSpeed - (int)( rampTimeGap * rampRatio ); 
              }else{
                speedDelay = maxSpeed;
              }      
            return;
            }
        }
=======
  infinity = 0;
  status = 's';
  rampCounter = 0;
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
}



<<<<<<< HEAD

/*
 * SET DIRECTION
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::setStepDirection( bool dirLoc ){
    stepper.setDirection( dirLoc );
} 



<<<<<<< HEAD

/*
 * READ CURRENT MOTOR POSIZTION
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::readPosition(){ 
  Serial.println(steps);
}



<<<<<<< HEAD

/*
 * READ CURRENT MOTOR STATUS
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::readStatus(){  
  Serial.println(status);
}


<<<<<<< HEAD


/*
 * READ MOTOR STATUS AND POSITION
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::readAll(){
  Serial.print(status);
  Serial.print(" ");
  Serial.println(steps);
}



<<<<<<< HEAD

/*
 *  CHECK IF MOTOR IS IN HOME POSITION
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::isInHome(){
  
   if( digitalRead( switchHome ) ){
      steps = 0;
      homePosition = false;
      limit = true;
      stop();
   }

   if( targetStep == 0 ){
     targetStep = ( COMPLETE_ROTATE + 2 );
     setStepDirection( 1 );
     dir = 1;     
   }

   
}



<<<<<<< HEAD


/*
 *  READ ALL EEPROM SETTINGS
 */
void Rotatore::getEeprom()
{  
      reloadEeprom();
      Serial.print("max: ");
      Serial.print(maxSpeed);
      Serial.print("| ini: ");
      Serial.print(initialSpeed);
      Serial.print("| steps: ");
      Serial.print(steps);
      Serial.print("| backlash: ");
      Serial.print(backlash);
      Serial.print("| rampTime: ");
      Serial.println(rampTime);
}




/*
 *  LOAD EEPROM TO ROUTIN DATA
 *  12:max ; 14:ini ; 16:steps ; 18:backlash; 20:rampTIme; 22:oldDir
 */
void Rotatore::reloadEeprom()
{  
      maxSpeed      = EEPROMReadInt( 12 );
      initialSpeed  = EEPROMReadInt( 14 );  
      steps         = EEPROMReadInt( 16 );
      backlash      = EEPROMReadInt( 18 );
      rampTime      = EEPROMReadInt( 20 );
}



/*
 * SET EEPROM MAX SPEED
 */
void Rotatore::setEepromMaxVel( int value )
{ 
      //limite fisico sulla velocitÃ 
      if( value < LIMIT_FOC_MAX_SPEED ){
        value = LIMIT_FOC_MAX_SPEED;
      }
      //se la velocitÃ  massima Ã¨ maggiore dell'iniziale, prendo l'iniziale, sennÃ² rallenterebbe
      if( value > LIMIT_FOC_INI_SPEED ){
        value = LIMIT_FOC_INI_SPEED;
      }
      EEPROMWriteInt( 12, value );
      maxSpeed = value;
}



/*
 * SET EEPROM INITIAL SPEED
 */
void Rotatore::setEepromIniVel( int value )
{  
      //limite fisico sulla velocitÃ  iniziale
      if( value < LIMIT_FOC_INI_SPEED ){
        value = LIMIT_FOC_INI_SPEED;
      }
      //se la velocitÃ  iniziale Ã¨ minore della massima, prendo la massima, sennÃ² rallenterebbe      
      if( value < LIMIT_FOC_MAX_SPEED ){
        value = LIMIT_FOC_MAX_SPEED;
      }
      EEPROMWriteInt( 14, value );
      initialSpeed = value;
}



/*
 * SET EEPROM BACKSLASH
 */
void Rotatore::setEepromBacklash( int value )
{  
      EEPROMWriteInt( 18, value );
      backlash = value; 
}



/*
 * SET EEPROM ACCELERATION TIME
 */
=======
// 12:max ; 14:ini ; 16:steps ; 18:backlash; 20:rampTIme; 22:oldDir
void Rotatore::getEeprom(){
  maxSpeed = EEPROMReadInt( 12 );
  initialSpeed = EEPROMReadInt( 14 );  
  steps = EEPROMReadInt( 16 );
  backlash = EEPROMReadInt( 18 );
  rampTime = EEPROMReadInt( 20 );
  Serial.print("max: ");
  Serial.print(maxSpeed);
  Serial.print("| ini: ");
  Serial.print(initialSpeed);
  Serial.print("| steps: ");
  Serial.print(steps);
  Serial.print("| backlash: ");
  Serial.print(backlash);
  Serial.print("| rampTime: ");
  Serial.println(rampTime);
}


void Rotatore::setEepromMaxVel( int value ){
  EEPROMWriteInt( 12, value );
}


void Rotatore::setEepromIniVel( int value ){
  EEPROMWriteInt( 14, value );
}


void Rotatore::setEepromBacklash( int value ){
  EEPROMWriteInt( 18, value );
}


>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::setEepromRampTime( int value ){
      EEPROMWriteInt( 20, value );
      rampTime = value;
}


<<<<<<< HEAD

/*
 * SET EEPROM OLD DIRECTION
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::setEepromOldDir( int value ){
  EEPROMWriteInt( 22, value );
}


<<<<<<< HEAD

/*
 * WRITE INT TO EEPROM
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
void Rotatore::EEPROMWriteInt(int address, int value)
{
      byte two = (value & 0xFF);
      byte one = ((value >> 8) & 0xFF);

      EEPROM.write(address, two);
      EEPROM.write(address + 1, one);
}


<<<<<<< HEAD

/*
 * READ INT FROM EEPROM
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
int Rotatore::EEPROMReadInt(int address)
{
      int two = EEPROM.read(address);
      int one = EEPROM.read(address + 1);
      return ((two << 0) & 0xFF) + ((one << 8) & 0xFFFF);
}



<<<<<<< HEAD
/*
 * CHECK THAT THE WIRES DON'T TWIST
 */
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
long int Rotatore::checkWire(long int voglio, bool dirLoc){
  voglio =  voglio % ( COMPLETE_ROTATE );
  if( dirLoc == 1 ){
      if( ( voglio + steps ) >= ( COMPLETE_ROTATE / 2 ) ){
        targetStep = ( COMPLETE_ROTATE - voglio );
        setStepDirection( 0 );
        dir = 0;
      }
      else{
        targetStep = voglio;
        dir = dirLoc;
      }
  }
  else{
      if( ( voglio - steps ) >= ( COMPLETE_ROTATE / 2 ) ){
        targetStep = ( COMPLETE_ROTATE - voglio );
        setStepDirection( 1 );
        dir = 1;
      }
      else{
        targetStep = voglio;
        dir = dirLoc;
      }
  }
  return targetStep;
}
/*bool Rotatore::checkError(){
  statusError = digitalRead( error );
  if( oldError != statusError ){
    oldError = statusError;
    return true;
  }
  return false;
}*/



/*
 * INIT ACCELERATION DATA
 */
void Rotatore::initRampData()
{
      rampRatio = 0;
      rampTimeMicro = 0;
      rampTimeStart = 0;
      
      // Se non ho un tempo di rampa ritorno
      if( rampTime == 0 ){
        speedDelay = maxSpeed; 
      }else{
        speedDelay = initialSpeed; 
      }
  
      // Trasformo il valore salvato in secondi in microsecondi
      unsigned long int rampTimeMicro = rampTime*1000000; 
  
      // Ottengo il gap di velocitÃ  (inteso in microsecondi di frequenza) da dover compensare
      long int speedGap = initialSpeed - maxSpeed;
        
      // Se non c'Ã¨ molta differenza tra le velocitÃ  ritorno
      if( speedGap < 20 ) return;
  
      //Calcolo il rapporto tra temp T1 (tempo di avvio della rampa) e il T2 (tempo di down del segnale di controllo)
      rampRatio = (double)speedGap / (double)rampTimeMicro;
  
      rampTimeStart = micros();
}



/*
 * READ ERROR FROM DRIVER
 */
void Rotatore::readError(){
  return;
  uint16_t flags = stepper.readNonLatchedStatusFlags();
  if (flags)
  {
    if (flags & AMIS30543::OPENY)
    {
            Serial.println("OPENY");
    }
    if (flags & AMIS30543::OPENX)
    {
            Serial.println("OPENX");
    }
    if (flags & AMIS30543::WD)
    {
            Serial.println("WD");
    }
    if (flags & AMIS30543::CPFAIL)
    {
            Serial.println("CPFAIL");
    }
    if (flags & AMIS30543::TW)
    {
            Serial.println("TW");
    }
  }
}

