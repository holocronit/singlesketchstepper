/* ====== REQUIRED LIBRARIES ====== */
#include <SoftwareSerial.h>
#include "config.h"
#include "FocheggiatoreLib.h"
#include "RotatoreLib.h"
#include <SerialCommand.h>



/* ====== CLASS INIT ====== */
SerialCommand SCmd;

<<<<<<< HEAD
Focheggiatore   foc = Focheggiatore(  PIN_FOC_FC_UP, PIN_FOC_STEP, PIN_FOC_SLAVE, PIN_FOC_FC_UP, PIN_FOC_FC_DOWN, PIN_FOC_HOME );

Rotatore        rot = Rotatore(       PIN_ROT_HOME, PIN_ROT_STEP, PIN_ROT_SLAVE, PIN_ROT_HOME );
=======
Focheggiatore   foc = Focheggiatore(  PIN_FOC_DIR, PIN_FOC_STEP, PIN_FOC_SLAVE, PIN_FOC_FC_UP, PIN_FOC_FC_DOWN, PIN_FOC_HOME );

Rotatore        rot = Rotatore(       PIN_ROT_DIR, PIN_ROT_STEP, PIN_ROT_SLAVE, PIN_ROT_HOME );
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1



/* ====== MAIN SETUP ====== */
void setup()
{
  // Inizializzo Seriale
  Serial.begin( SERIAL_BOUND_RATE );
  
  // Inizializzo Focheggiatore
  foc.begin();
  
  // Inizliazzo Rotatore
  rot.begin();

  // Registro comandi seriale
  SCmd.addCommand("CMM", motori);
  SCmd.addCommand("LMP", lettPosizione);
  SCmd.addCommand("CMI", infinito);
  SCmd.addCommand("CMS", stopp);
  SCmd.addCommand("LMO", lettTuttePosizioni);
  SCmd.addCommand("LMS", lettStatus);
  SCmd.addCommand("LMG", lettTutto);
  SCmd.addCommand("SET", setParameter);
  SCmd.addCommand("GET", getParameter);
  SCmd.addCommand("GOTO", goToPosition);
  
  // Risposta di defaut in caso di comando non riconosciuto
  SCmd.addDefaultHandler(unrecognized);
}


/* ====== MAIN LOOP ====== */
void loop() {
  // Handler comandi seriale
  SCmd.readSerial();
  // Routine per Focheggiatore
  foc.singleSteps();
  // Routine per Rotatore
  rot.singleSteps();
}


/* ====== CMM ====== */
void motori() {

  long int steps;
  char* stepsSerial;

  char* id  = SCmd.next();
  char* dir = SCmd.next();

  if ( *dir == KEY_FORWARD || *dir == KEY_BACKWARD ) {

    char* stepsSerial;
    stepsSerial = SCmd.next();
    String appoSteps = stepsSerial;
    steps =  appoSteps.toInt();

  }
<<<<<<< HEAD
  
=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1

  if ( *id == FOC_MOT_ID ) {
    foc.move( *dir, steps );
  }
  else if ( *id == ROT_MOT_ID ) {
    rot.move( *dir, steps );
  }

  Serial.println("ok-CMM");
}

/* ====== LMP ====== */
void lettPosizione() {
  char* id = SCmd.next();
  Serial.print("ok-LMP|");
  if ( *id == FOC_MOT_ID ) {
    foc.readPosition();
  }
  else if ( *id == ROT_MOT_ID ) {
    rot.readPosition();
  }
}


/* ====== CMI ====== */
void infinito() {
  char* id = SCmd.next();
  char* dir = SCmd.next();

  if ( *id == FOC_MOT_ID ) {
    foc.move( *dir, 200 , 1 );
  }
  else if ( *id == ROT_MOT_ID ) {
    rot.moveInf( *dir );
  }
  Serial.println("ok-CMI");
}


/* ====== CMS ====== */
void stopp() {
  char* id = SCmd.next();
  if ( *id == FOC_MOT_ID ) {
    foc.stop();
  }
  else if ( *id == ROT_MOT_ID ) {
    rot.stop();
  }
  Serial.println("ok-CMS");
}


/* ====== LMO ====== */
void lettTuttePosizioni() {
  Serial.print("ok-LMO|");
  foc.readPosition();
  Serial.print("|");
  rot.readPosition();
}


/* ====== LMS ====== */
void lettStatus() {
  Serial.print("ok-LMS|");
  foc.readStatus();
  Serial.print("|");
  rot.readStatus();
}


/* ====== LMG ====== */
void lettTutto() {
  Serial.print("ok-LMG|");
  foc.readAll();
  Serial.print("|");
  rot.readAll();
}


/* ====== SET ====== */
void setParameter() {

  String func;
  int vel;
  char maxVel[] = "max";
  char minVel[] = "ini";
  char blash[] = "bla";
  char ramp[] = "ramp";
  char* id = SCmd.next();
  func = SCmd.next();
  char* valueSerial = SCmd.next();
  vel = convertToInt( valueSerial );
  if ( *id == '1' ) {
    if ( func == maxVel ) {
      foc.setEepromMaxVel(vel);
<<<<<<< HEAD
    }
    else if ( func == minVel ) {
      foc.setEepromIniVel(vel);
    }
    else if ( func == blash ) {
      foc.setEepromBacklash(vel);
    }
=======
    }
    else if ( func == minVel ) {
      foc.setEepromIniVel(vel);
    }
    else if ( func == blash ) {
      foc.setEepromBacklash(vel);
    }
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
    else if ( func == ramp ) {
      foc.setEepromRampTime(vel);
    }
  }
  if ( *id == '2' ) {
    if ( func == maxVel ) {
      rot.setEepromMaxVel(vel);
    }
    else if ( func == minVel ) {
      rot.setEepromIniVel(vel);
<<<<<<< HEAD
    }
    else if ( func == blash ) {
      rot.setEepromBacklash(vel);
    }
=======
    }
    else if ( func == blash ) {
      rot.setEepromBacklash(vel);
    }
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
    else if ( func == ramp ) {
      rot.setEepromRampTime(vel);
    }
  }
  Serial.println("ok-SET");
}


/* ====== GET ====== */
void getParameter() {
  char* id = SCmd.next();
  Serial.print("ok-GET|");
  if ( *id == '1' ) {
    foc.getEeprom();
  }
  if ( *id == '2' ) {
    rot.getEeprom();
  }

}


/* ====== GOTO ====== */
void goToPosition() {
  long int pos;
  char* posSerial;
  char* id = SCmd.next();
  posSerial = SCmd.next();
  pos = convertToInt( posSerial );
  if ( *id == '1' ) {
    foc.goTo( pos );
  }
  else if ( *id == '2' ) {
    rot.goTo( pos );
  }
  Serial.println("ok-GOTO");
}


/* ====== DEFAULT ====== */
void unrecognized()
{
  Serial.println("error");
}


/* ====== HELPERS ====== */
long int convertToInt( char* vett ) {
  String appoString = vett;
  long int num =  appoString.toInt();
  return num;
}


<<<<<<< HEAD


=======
>>>>>>> 04ec34b81e625b54ffd82237cef6047106bcf8a1
